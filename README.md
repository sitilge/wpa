# wpa

WPA supplicant initialization and command line scan scripts.

## Usage

Enter the name of the interface as the first argument for the `init.sh` script. Then run the `scan.sh` to scan for the available networks.

## Example

````
# ./init.sh wlp3s0b1
# ./scan.sh
````
